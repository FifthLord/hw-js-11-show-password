
let passwordForm = document.querySelector('.password-form');
passwordForm.addEventListener('click', changeType);
passwordForm.addEventListener('submit', comparePassword);


function changeType(event) {
   if (event.target.closest('.fas')) {
      event.target.classList.toggle('fa-eye');
      event.target.classList.toggle('fa-eye-slash');
      if (event.target.classList.contains('fa-eye')) {
         event.target.parentNode.getElementsByTagName('input')[0].setAttribute('type', 'password');
      } else {
         event.target.parentNode.getElementsByTagName('input')[0].setAttribute('type', 'text');
      }
   }
}


function comparePassword(event) {
   event.preventDefault();
   let basic = document.getElementById('basic').value;
   let checking = document.getElementById('checking').value;

   if (document.getElementById('err')) {
      document.getElementById('err').remove()
   }

   if (!basic && !checking) {
      document.getElementById('checking').insertAdjacentHTML('afterend', `<p id='err' style="color:red;">Потрібно ввести значення</p>`);
   } else if (basic === checking) {
      alert('You are welcome')
   } else {
      document.getElementById('checking').insertAdjacentHTML('afterend', `<p id='err' style="color:red;">Потрібно ввести однакові значення</p>`);
   }
}